# This Dockerfile creates a static build image for use in pipelines.
# Using the apline image lets us get a very slim Linux version without any extra
# packages.
FROM python:3.12-rc-alpine3.17

WORKDIR /usr/src/app

# Copy our requirements file so we can instrall dependencies.
COPY requirements.txt ./
# Use pip to install any dependencies.
RUN pip install --no-cache-dir -r requirements.txt

# Now copy all of our code.
COPY ./src .

# In this case, our application is a script that requires arguments to run.  We don't need to have
# the image start the application, rather we just need Python and all our dependencies installed.