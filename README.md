# Bootstrap

[[_TOC_]]

---

## Purpose

This project was born out of a frustration with how long it takes to truly set up all the various integrations between services like GitLab, Jira, SonarCloud, Azure DevOps, and so on.  Simply creating a new repository is simple, but getting a bare-bones projects up an running using a full CI/CD suite takes a much more concerted effort.  Therefore, the purpose of Bootstrap is to simplify creating a new repository so that it has all of the necessary pipelines, branch policies, integrations, and deployment settings needed.

Bootstrap is there to help you get straight to coding, not babysitting settings pages.

---

## Usage

TBD once this has been fleshed out.

---

## Architecture

TBD once this has been fleshed out.

---

## Important Files

### flake8

The [.flake8](.flake8) file is used to configure code style and linting for the project.  It exists at the top level directory to handle linting for all files in any subfolders as well.

### requirements.txt

The [requirements.txt](requirements.txt) file defines all of the python packages that are dependencies for both running unit tests and and running the application.

### gitlab-ci.yml

The [.gitlab-ci.yml](.gitlab-ci.yml) file handles the pipeline for the project, such as the unit tests and code quality scanning.

---
