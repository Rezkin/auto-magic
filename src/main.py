# Python built-ins.
import argparse
import pdb
import sys
import requests
import pprint

# Status codes
SUCCESS = 0


def post_mortem_exception_handler(exception_type, exception,
                                  traceback, except_hook=sys.excepthook) -> None:
    """Starts a post-mortem debug shell at the point of failure.

    This processes exceptions normally in all other ways.

    Args:
        exception_type (Type[BaseException]): Exception type being raised.
        exception (BaseException): Exception instance being raised.
        traceback (TracebackType): Traceback of the exception which is used to open a debug shell.

    KArgs:
        except_hook (Function, optional): Exception handler to add debugging to.
        Defaults to sys.excepthook.
    """
    except_hook(exception_type, exception, traceback)
    pdb.post_mortem(traceback)


def main() -> int:
    """The entry point into the application which handles argument parsing and running the
    appropriate logic.

    Returns:
        int: The status code of the application to be returned to the system.
    """
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-d", "--debug", required=False, action="store_true",
                        help="Enables a PDB post-mortem traceback for diagnosing and debugging " +
                        "errors.")
    parser.add_argument("--token", required=True, type=str,
                        help="The GitLab Personal Access Token to use for authentication.")
    parser.add_argument("--name", required=True, type=str,
                        help="The name of the new repository.")

    # Parse the arguments into argparse's namespace, and then turn them into a dictionary.
    # This way, we can pop arguments off and pass around the whole set to classes via *args or
    # **kargs if need be.
    args_dict = vars(parser.parse_args())

    # If desired, set the system's exception handler to the post-mortem handler for easier
    # debugging.
    if args_dict.get("debug"):
        sys.excepthook = post_mortem_exception_handler

    host = "https://gitlab.com/"
    api_base = "api/v4/"
    api_path = "projects"
    url = host + api_base + api_path + "?private_token=" + args_dict["token"]
    data = {
        "name": args_dict["name"]
    }
    result = requests.post(url, json=data)

    pprint.pprint(result.text)

    return SUCCESS


# Only run main if we are executing this file directly.  Without putting it in a function, any code
# inside this file would also be run if it is imported.  This if statement checks for that.
if __name__ == "__main__":
    sys.exit(main())
